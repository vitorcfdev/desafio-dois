package jogos.lpa;
import java.util.Scanner; 

public class desafio2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);

	    String nome, classe; 
	    double notaP1,notaP2,notaP3, notaI1, notaI2, notaI3, notaH1,notaH2, notaH3, notaG1, notaG2, notaG3;
	    double mediaP, mediaI, mediaH, mediaG;
	    final int n = 3;

	    System.out.println("Informe seu nome:");
	    nome = input.next();
	    System.out.println("Informe sua classe:");
	    classe = input.next();
	    System.out.println("Informe as 3 notas de Português:");
	    notaP1 = input.nextDouble();
	    notaP2 = input.nextDouble();
	    notaP3 = input.nextDouble(); 
	    mediaP = (notaP1 + notaP2 + notaP3) / n;
	    System.out.println("Informe as 3 notas de Inglês:");
	    notaI1 = input.nextDouble();  
	    notaI2 = input.nextDouble(); 
	    notaI3 = input.nextDouble();  
	    mediaI = (notaI1 + notaI2 + notaI3) / n;
	    System.out.println("Informe as 3 notas de História:");
	    notaH1 = input.nextDouble();  
	    notaH2 = input.nextDouble();  
	    notaH3 = input.nextDouble(); 
	    mediaH = (notaH1 + notaH2 + notaH3) / n;
	    System.out.println("Informe as 3 notas de Geografia:");
	    notaG1 = input.nextDouble();  
	    notaG2 = input.nextDouble(); 
	    notaG3 = input.nextDouble(); 
	    mediaG = (notaG1 + notaG2 + notaG3) / 3; 
	    
	    System.out.println("Nome: " + nome + "\nClasse: "+ classe + "\nMédia Português: " + mediaP + "\nMédia Inglês: " + mediaI + "\nMédia História: " + mediaH + "\nMédia Geografia: " + mediaG);
	    
	}

}
